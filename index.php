<html>
   <head>
        <title>Login</title>
        <link rel="stylesheet" type="css" href="src/css/estilos.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous">
        </script>
    </head>
    
    <body>
        <div class="container">
            <form>
                <div class="main-div">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Insira o email" autofocus="">
                    </div>

                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" class="form-control" id="senha" placeholder="Insira a senha">
                    </div>
                    
                    <button type="submit" class="btn btn-primary" id="entrar">Entrar</button>
                    <button type="button" class="btn btn-secundary" onClick="parent.location.href='frmCadUsuario.php'">Criar uma conta</button>
                </div>
            </form>
        </div>
    </body>
</html>

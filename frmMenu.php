<html>
    <head>
        <link rel="stylesheet" type="text/css" href="src/css/estilos.css">
        <title>Menu</title>
    </head>
    <body>
        <table>
            <tr>
            </tr>
            <tr>
                <th>
                    <a href="#" class="menu-close">
                                <img src="src/img/menu.png" alt="Menu" class="rounded-circle">
                    </a>
                    <div class="side-menu-wrapper">
                        <ul>
                            <li><a href="frmAddTarefa.php" target="_self" rel="nofollow">Adicionar</a></li>
                            <li><a href="frmHistorico.php" target="_self" rel="nofollow">Historico</a></li>
                            <li><a href="frmSair.php" target="_self" rel="nofollow">Log out</a></li>
                        </ul>
                    </div>
                </th>
                <th>
                    <div class="container">
                        <?php
                            require_once 'tarefas.php';
                            $t= new tarefa();
                            $dados=$t->buscarTodas();
                            echo "<table class='table table-bordered'>";
                            foreach($dados as $linha){
                                print "<tr>";
                                print "<td>".$linha['descricao']."</td>";
                                print "</tr>";
                                print "<tr>";
                                print "<td>".$linha['data_hora']."</td>";
                                print "</tr>";
                                print "<tr>";
                                print "<td>".$linha['obs']."</td>";
                                print "</tr>";
                                print "<tr>";
                                print '<td><input class="btn btn-primary" type="submit" value="Editar"></td>';
                                print '<td><input class="btn btn-secundary" type="submit" value="Excluir"></td>';
                                print "</tr>";
                            }
                            echo "</table>";
                        ?>
                    </div>
                </th>
            </tr>
        </table>
    </body>
</html>